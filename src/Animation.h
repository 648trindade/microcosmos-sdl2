#ifndef __ANIMATION_H__
#define __ANIMATION_H__

#include <SDL2/SDL.h>
#include "Mask.h"

class Animation{
private:
    SDL_Texture* image;
    Mask* mask_player;
    SDL_Texture* actualImage;
public:
    Animation();
    ~Animation();
    void loadImages();
};

#endif