#include "Blocks.h"
#include "ImageControl.h"
#include <SDL2/SDL_image.h>

Blocks::Blocks(SDL_Point location, std::string image, SDL_Point axis){
    //pygame.sprite.Sprite.__init__(self) ???
    location = ImageControl::fixvalues(location);
    this->rect = SDL_Rect{location.x, location.y, axis.x, axis.y};
    //TODO: Precisa do renderer!!
    //this->block_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, axis.x, axis.y);
    this->define_img(image);
    this->image = ImageControl::fixScale(this->image);
    this->text.clean();
}

Blocks::~Blocks(){
    SDL_DestroyTexture(block_texture);
    SDL_DestroyTexture(image);
}

void Blocks::define_img(std::string image){
    SDL_Surface* TempSurface = IMG_Load(
        (std::string("../graphics/Plataformas/blocks/") + imagem + std::string(".png")).c_str()
    );
    if (TempSurface == nullptr)
        throw std::runtime_error(
            (std::string("[ERRO] Cannot load image file ") + image).c_str()
        );
    //TODO: Precisa do Renderer!!
    //this->image = SDL_CreateTextureFromSurface(renderer, TempSurface);
    SDL_FreeSurface(TempSurface);
}

void Blocks::defineImg(){}

void Blocks::loadText(){
    for(std::string s : text)
        return s; //??????
}

void renderBlocks(){
    defineImg(); //??????
}