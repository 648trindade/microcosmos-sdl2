#ifndef __BLOCKS_H__
#define __BLOCKS_H__

#include <vector>
#include <string>
#include <SDL2/SDL.h>
#include "Entity.h"

class Blocks : public Entity{
private:
    SDL_Rect rect;
    SDL_Texture* block_texture;
    SDL_Texture* image;
    std::vector<std::string> text;
public:
    Blocks(SDL_Point location, std::string image, SDL_Point axis);
    ~Blocks();
    void define_img(std::string image);
    void defineImg();
    void loadText();
    void renderBlocks();
};

#endif