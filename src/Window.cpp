#include "Window.h"
#include <algorithm>
#include <SDL2/SDL_image.h>

Window::Window(){
    icon = IMG_Load("../graphics/icon.jpg");
    getResolution();
    setImageResolution();
    setWindowScreen();
    SDL_SetWindowIcon(windowScreen, icon);
    resolutionChange = false;
}

Window::~Window(){
    SDL_FreeSurface(icon);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(windowScreen);
}

void Window::setFullScreen(){
    SDL_SetWindowFullscreen(windowScreen, SDL_WINDOW_FULLSCREEN);
}

void Window::setWindowScreen(){
    windowScreen = SDL_CreateWindow(
        "Microcosmos",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        1280, 720,
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN
    );

    renderer = SDL_CreateRenderer(windowScreen, -1, SDL_RENDERER_ACCELERATED))
}

void Window::getResolution(){
    screenResolution = SDL_Point{1280, 720};
}

SDL_Rect Window::camera(SDL_Rect camera, SDL_Rect target_rect){
    int l = target_rect.x;
    int t = target_rect.y;
    int w = camera.w;
    int h = camera.h;

    // retangulo da camera
    l = -l + getHalfWidth() / 2;
    t = -t + getHalfHeight();

    // para movimento na esquerda
    l = min(0, l);
    // para movimento na direita
    l = max(-(camera.w - screenResolution.x), l);

    // para movimento embaixo
    t = max(-(camera.h - screenResolution.y), t);

    //para movimento em cima
    t = min(0, t);

    return SDL_Rect{l, t, w, h};
}

int Window::getHalfWidth(){
    return screenResolution.x / 2;
}

int Window::getHalfHeight(){
    return screenResolution.y / 2;
}

void Window::setImageResolution(){
    ImageControl::setImageResolution(screenResolution);
    resolutionChange = true;
}

void Window::changeResolution(SDL_Event event){
    SDL_SetWindowSize(windowScreen, event.window.data1, event.window.data2);
}

void Window::changeDefinedResolution(SDL_Point resolution){
    SDL_SetWindowSize(windowScreen, resolution.x, resolution.y);
    screenResolution = resolution;
    setImageResolution();
}