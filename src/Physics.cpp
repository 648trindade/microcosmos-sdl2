#include "Physics.h"

Physics::Physics():
    constantGravity(0.2f), gravity(0.2f), maxDistance(25.f), vel_x(0.f),
    vel_y(0.f), vel_y_i(0.f), fall(true), constantJumpPower(5.f), jumpPower(5.f)
{}

void Physics::check_falling(){
    fall = vel_y < 0.f;
}

void Physics::update_physics(float fps){
    if (fall){
        float time = 60.f/fps;
        vel_y_i += gravity * time;
        vel_y = (vel_y_i * time) + ((gravity / 2.f) * (time * time));
        if (vel_y > maxDistance)
            vel_y = maxDistance
    }
    else{
        vel_y += vel_y_i;
        check_falling();
    }
}

void Physics::get_position(){
    if (!fall)
        check_falling();
}

void Physics::jump(){
    if (fall){
        vel_y = vel_y_i = -jumpPower;
        fall = false;
    }
}