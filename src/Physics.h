#ifndef ___PHYSICS_H__
#define ___PHYSICS_H__

class Physics {
private:
    float constantGravity;
    float gravity;
    float maxDistance;
    float vel_x;
    float vel_y;
    float vel_y_i;
    bool  fall;
    float constantJumpPower;
    float jumpPower;
public:
    Physics();
    void check_falling();
    void update_physics(float fps);
    void get_position();
    void jump();
};

#endif