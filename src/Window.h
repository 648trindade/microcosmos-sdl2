#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <SDL2/SDL.h>

class Window{
private:
    SDL_Texture* icon;
    bool resolutionChange;
    SDL_Window* windowScreen;
    SDL_Renderer* renderer;
    SDL_Point screenResolution;
public:
    Window();
    ~Window();
    void setFullScreen();
    void setWindowScreen();
    void getResolution();
    SDL_Rect camera();
    int getHalfWidth();
    int getHalfHeight();
    void setImageResolution();
    void changeResolution(SDL_Event);
    void changeDefinedResolution(SDL_Point);
};

#endif